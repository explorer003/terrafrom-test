variable "stage" {
  description = "Name of stage"
  default     = "sandbox"
  type        = string
}
variable "name" {
  description = "Name of vpc for GKE"
  default     = "equeum-staging-cluster"
  type        = string
}
variable "gcloud_project" {
  description = "equeum-devops-sandbox"
  default     = "equeum-devops-sandbox"
  type        = string
}
variable "location" {
  description = "The location in which the cluster master will be created(zone or region)"
  default     = "us-east1-b"
  type        = string
}
variable "remove_default_node_pool" {
  description = "Use default node pool or seperately managed node pool"
  default     = true
  type        = bool
}
variable "initial_node_count" {
  description = "The number of nodes to create in this cluster's default node pool(when remove_default_node_pool = true, this value will be `1`)"
  default     = 3
  type        = number
}
variable "vpc_link" {
  description = "Link of vpc to which the cluster is connected"
  default     = ""
  type        = string
}
variable "subnet_link" {
  description = "Name of subnet for GKE"
  default     = ""
  type        = string
}
variable "gke_master_version" {
  description = "Version of GKE cluster"
  default     = ""
  type        = string
}

variable "addons_config" {
  description = "Addons config for GKE cluster"
  default     = null
  type = list(object({
    disable_load_balancing     = bool
    disable_horiz_pod_autoscal = bool
    disable_istio_config       = bool
  }))
}
# services - The name of the existing secondary range in the cluster's subnetwork to use for service ClusterIP
# cluster - The name of the existing secondary range in the cluster's subnetwork to use for pod IP addresses
variable "ip_policy" {
  description = "Configuration of cluster IP allocation for VPC-native clusters."
  default     = null
  type = list(object({
    services_sec_range_name = string
    cluster_sec_range_name  = string
  }))
}
# Node Pools
variable "node_pools" {
  type        = list(map(string))
  description = "List of maps containing node pools"

  default = [
    {
      name = "alex-engine-high-cpu-mem"
      name = "compute-intensive"
      name = "memory-intensive"
      
    },
  ]
}
